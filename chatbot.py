#######################################################
## Modules
#######################################################

# various incantations to minimize tensorflow's complaints
import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# things needed for Tensorflow
import numpy as np
import tensorflow as tf

# incantation to minimize tflearn's complaints
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

# tflearn, which is used as interface to tensorflow, that handles the neural net
import tflearn

# various incantations to minimize nltk's complaints
import io
from contextlib import redirect_stdout, redirect_stderr
f = io.StringIO()
g = io.StringIO()
with redirect_stdout(f):
    with redirect_stderr(g):
        # things we need for NLP
        import nltk
        nltk.download('punkt')
        from nltk.stem.lancaster import LancasterStemmer
        stemmer = LancasterStemmer()

import random  # for making excellent choices
import json    # for importing intents file
import pickle  # for storage and retrieval of shared data structures

#######################################################
## Helper Functions
#######################################################

# question->response patterns to use for training
def load_intents():
    with open('intents.json') as json_data:
        intents = json.load(json_data)
    return intents

def retrieve_training_data():
    data = pickle.load( open( "model/training_data", "rb" ) )
    return data['words'], data['classes'], data['train_x'], data['train_y']

def store_training_data(words, classes, train_x, train_y):
    pickle.dump( {'words':words, 'classes':classes, 'train_x':train_x, 'train_y':train_y}, open( "model/training_data", "wb" ) )


#######################################################
## Model Generation
#######################################################

def synthesize_training_data():
    words = []
    classes = []
    documents = []
    ignore_words = ['?']
    intents = load_intents()

    # loop through each sentence in our intents patterns
    for intent in intents['intents']:
        for pattern in intent['patterns']:
            w = nltk.word_tokenize(pattern)
            words.extend(w)
            documents.append((w, intent['tag']))
            if intent['tag'] not in classes:
                classes.append(intent['tag'])

    # stem and lower each word and remove duplicates
    words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
    words = sorted(list(set(words)))

    # remove duplicates
    classes = sorted(list(set(classes)))

    print (len(documents), "documents")
    print (len(classes), "classes", classes)
    print (len(words), "unique stemmed words", words)

    return words, documents, classes

def training_data_to_tensors(words, documents, classes):
    # create training data
    training = []
    output = []
    # create an empty array to use as output layer
    output_empty = [0] * len(classes)

    # training set, bag of words for each sentence
    for doc in documents:
        # initialize bag of words
        bag = []
        # list of tokenized words for the pattern
        pattern_words = doc[0]
        # stem each word
        pattern_words = [stemmer.stem(word.lower()) for word in pattern_words]
        # create bag of words array
        for w in words:
            bag.append(1) if w in pattern_words else bag.append(0)

        # output is a '0' for each tag and '1' for current tag
        output_row = list(output_empty)
        output_row[classes.index(doc[1])] = 1

        training.append([bag, output_row])

    # shuffle features and turn into np.array
    random.shuffle(training)
    training = np.array(training, dtype=object)

    # create train and test lists
    train_x = list(training[:,0])
    train_y = list(training[:,1])
    print("Train_x and _y:")
    print(train_x[0])
    print(train_y[0])

    return train_x, train_y

def build_model():
    # reset underlying graph data
    tf.compat.v1.reset_default_graph()

    # retrieve stored training data
    words, classes, train_x, train_y = retrieve_training_data()

    # build neural network
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    layer_size = 12 # 8
    net = tflearn.fully_connected(net, layer_size)
    net = tflearn.fully_connected(net, layer_size)
    net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
    net = tflearn.regression(net)

    # define model and set up tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_logs')
    # start training (apply gradient descent algorithm)
    model.fit(train_x, train_y, n_epoch=1000, batch_size=8, show_metric=True)
    model.save('model/model.tflearn')

def generate_model():
    words, docs, classes = synthesize_training_data()
    train_x, train_y = training_data_to_tensors(words, docs, classes)
    store_training_data(words, classes, train_x, train_y)
    build_model()
    print()
    print("Training summary:")
    print (len(docs), "documents")
    print (len(classes), "classes", classes)
    print (len(words), "unique stemmed words", words)
    print("Input layer size:", len(train_x[0]))
    print("Output layer size:", len(train_y[0]))


#######################################################
# Running the Chatbot
#######################################################

# conversational context for each user
context = {}
ERROR_THRESHOLD = 0.80

def load_model():
    # reset underlying graph data
    tf.compat.v1.reset_default_graph()

    # retrieve stored training data
    words, classes, train_x, train_y = retrieve_training_data()

    # build neural network
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    net = tflearn.fully_connected(net, 12) # 8
    net = tflearn.fully_connected(net, 12)
    net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
    net = tflearn.regression(net)

    # define model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_logs')

    # load saved model
    model.load('./model/model.tflearn')
    return model

# produce bag of words
def clean_up_sentence(sentence):
    # tokenize the pattern
    sentence_words = nltk.word_tokenize(sentence)
    # stem each word
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
    return sentence_words

# return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
def bow(sentence, words, show_details=False):
    # tokenize the pattern
    sentence_words = clean_up_sentence(sentence)
    # bag of words
    bag = [0]*len(words)
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s:
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)

    return(np.array(bag))

# processing questions/sentences and choosing responses
def classify(sentence, words, classes, model):
    # generate probabilities from the model
    results = model.predict([bow(sentence, words)])[0]
    # filter out predictions below a threshold
    results = [[i,r] for i,r in enumerate(results) if r>ERROR_THRESHOLD]
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append((classes[r[0]], r[1]))
    # return tuple of intent and probability
    return return_list

def response(sentence, intents, words, classes, model, userID='123', show_details=False):
    print('Sp:', sentence, flush=True)
    results = classify(sentence, words, classes, model)
    resp = "" # as unrecognized questions can occur, assume that no valid response will be found

    # if we have a classification then find the matching intent tag
    if results:
        # loop as long as there are matches to process
        while results:
            for i in intents['intents']:
                # find a tag matching the first result
                if i['tag'] == results[0][0]:
                    # set context for this intent if necessary
                    if 'context_set' in i:
                        if show_details: print ('context:', i['context_set'], flush=True)
                        context[userID] = i['context_set']

                    # check if this intent is contextual and applies to this user's conversation
                    if not 'context_filter' in i or \
                        (userID in context and 'context_filter' in i and i['context_filter'] == context[userID]):
                        if show_details:
                            print ('tag:', i['tag'], flush=True)
                        # a random response from the intent
                        resp = random.choice(i['responses'])
            results.pop(0)

    if not resp: # ensure that sensible response is produced, even if none was found
        resp = "Spørgsmålet blev desværre ikke forstået - simple spørgsmål inden for emnerne nævnt øverst vil oftest fungere. Skriv evt 'Information mangler her!' hvis du er overbevist om at svaret burde findes her, så kigger vi på det."
    print('Sv:', resp, flush=True)
    return resp
